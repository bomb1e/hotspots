<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Home | Internet Hotspot</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="./assets/bootstrap/bootstrap.min.css">
    <style>
        html, body {
            margin: 0;
            padding: 0;
        }
        body {
            padding: 70px 0;
        }
        .center-block {
            display: block;
            margin: 0 auto;
        }
    </style>
</head>

<body>
    <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <div class="container">
        <div class="row">
            <div>
                <nav class="navbar navbar-default navbar-fixed-top" style="background: white; color: #444;">
                    <div class="container">
                        <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="login.php">
                            <img src="./assets/logo.png" height="100%">
                        </a>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="login.php">Home</a></li>
                        </ul>
                        </div><!--/.nav-collapse -->
                    </div>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="jumbotron" style="background: #3477b4; color: white;">
                    <p>
                        Welcome to <br><b>Wiwi Hotspot Services.</b><br> Experience unlimited internet by selecting one of the packages below
                    </p>
                </div>
                <h4 class="text-center">Select Unlimited Package</h4>
                <form class="col-md-6 col-md-offset-3" action="pay.php" method="get">
                    <div class=" list-group">
                        <div class="form-group list-group-item">
                            <label class="label-inline">1 day @ KES 150</label>
                            <input class="pull-right" type="radio" name="package" value="150" required>
                        </div>
                        <div class="form-group list-group-item">
                            <label class="label-inline">1 week @ KES 400</label>
                            <input class="pull-right" type="radio" name="package" value="400" required>
                        </div>
                        <div class="form-group list-group-item">
                            <label class="label-inline">1 month @ KES 1000</label>
                            <input class="pull-right" type="radio" name="package" value="1000" required>
                        </div>
                    </div>
                    <button class="btn btn-success btn-lg btn-block" type="submit">Pay via Mpesa</button>
                    <a class="btn btn-default btn-lg btn-block" style="font-size: 1em;" href="./login.php">Have you already paid? Log In Here</a>
                </form>
            </div>
        </div>
    </div>
</body>
<script src="./assets/jquery.min.js"></script>
<script src="./assets/bootstrap/bootstrap.min.js"></script>

</html>
<?php
    session_start();
    if(isset($_POST) && !empty($_POST)) {
        $_SESSION['mac'] = $_POST['mac'];
        $_SESSION['ip'] = $_POST['ip'];
        $_SESSION['username'] = $_POST['username'];
        $_SESSION['link-login'] = $_POST['link-login'];
        $_SESSION['link-orig'] = $_POST['link-orig'];
        $_SESSION['error'] = $_POST['error'];
        $_SESSION['chap-id'] = $_POST['chap-id'];
        $_SESSION['chap-challenge'] = $_POST['chap-challenge'];
        $_SESSION['link-login-only'] = $_POST['link-login-only'];
        $_SESSION['link-orig-esc'] = $_POST['link-orig-esc'];
        $_SESSION['mac-esc'] = $_POST['mac-esc'];
    }

    $mac=$_SESSION['mac'];
    $ip=$_SESSION['ip'];
    $username=$_SESSION['username'];
    $linklogin=$_SESSION['link-login'];
    $linkorig=$_SESSION['link-orig'];
    $error=$_SESSION['error'];
    $chapid=$_SESSION['chap-id'];
    $chapchallenge=$_SESSION['chap-challenge'];
    $linkloginonly=$_SESSION['link-login-only'];
    $linkorigesc=$_SESSION['link-orig-esc'];
    $macesc=$_SESSION['mac-esc'];
?>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="expires" content="-1" />

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
        <title>WIwi Login</title>
        <!--REQUIRED STYLE SHEETS-->
        <!-- BOOTSTRAP CORE STYLE CSS -->
        <link href="assets/css/bootstrap.css" rel="stylesheet" />
        <!-- FONTAWESOME STYLE CSS -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLE CSS -->
        <link href="assets/css/style.css" rel="stylesheet" />
        <!-- GOOGLE FONT -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
        <!--<link rel="stylesheet" type="text/css" href="./assets/bootstrap/bootstrap.min.css">-->
    <link rel="stylesheet" type="text/css" href="./assets/style.css">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    </head>

    <body>
        <form name="sendin" action="<?php echo $linkloginonly; ?>" method="post">
        <input type="hidden" name="username" />
        <input type="hidden" name="password" />
        <input type="hidden" name="dst" value="<?php echo $linkorig; ?>" />
        <input type="hidden" name="popup" value="true" />
    </form>
<script type="text/javascript" src="./md5.js"></script>
    <script type="text/javascript">
        function doLogin() {
                <?php if(strlen($chapid) < 1) echo "return true;\n"; ?>
        document.sendin.username.value = document.login.username.value;
        document.sendin.password.value = hexMD5('<?php echo $chapid; ?>' + document.login.password.value + '<?php echo $chapchallenge; ?>');
        document.sendin.submit();
        return false;
        }
    </script>


        <!-- Navigation -->
        <header>
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                        <a class="navbar-brand" href="hotspot.goldandspace.co.ke">WIwi Hotspot Services </a>
                    </div>
                    <!-- Collect the nav links for toggling -->
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#home">HOME</a>
                            </li>
                            <li><a href="#services">OUR SERVICES</a>
                            </li>

                            <li><a href="#price-sec">OUR PACKAGES</a>
                            </li>

                            <li><a href="#contact-sec">CONTACT US</a>
                            </li>

                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container -->
            </nav>
        </header>
        <!--End Navigation -->




        <!--Header section  -->
        <div id="home">
            <div class="container">
                <div class="row ">
                    <div class="col-md-9 col-sm-9">
                        <h1 class="head-main">G&S NetworkS</h1>
                        <span class="head-sub-main">The No.1 Network in Lukenya</span>
                        <div class="head-last">

                            Get Hotspot Connection as low as possible rates. Nobody competes with us. Get Dedicated Connection from 150 Ksh per Mb
                        </div>

                    </div>
                    <div class="col-md-3 col-sm-3">
                        <div class="div-trans text-center">
                            <h3>Login to Access Internet </h3>
                             <?php if(isset($error) && !empty($error)) { ?>
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?php echo $error; ?>
                            </div>
                            <?php } ?>
                            <form class="col-md-12" name="login" action="<?php echo $linkloginonly; ?>" method="post" onSubmit="return doLogin()">
                                <div class="panel-body">
                                    <input type="hidden" name="dst" value="<?php echo $linkorig; ?>"/>
                                    <input type="hidden" name="popup" value="true" />

                                    <div class="form-group">
                                        <label>Username</label>
                                        <input name="username" class="form-control" type="text" value="<?php echo $username; ?>" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input class="form-control" name="password" type="password" required/>
                                    </div>
                                    <button class="btn btn-success btn-lg btn-block" value="OK" type="submit">LOGIN</button>
                                </div>
                            </form>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <!--End Header section  -->
        <!--Services Section-->
        <section id="services">
            <div class="container">

                <div class="row text-center">
                    <div class="col-md-8 col-md-offset-2">
                        <h2>Our Services</h2>

                    </div>

                </div>

                <div class="row text-center space-pad">
                    <div class="col-md-3 col-sm-3">

                        <div>

                            <i class="fa fa-signal fa-4x main-color"></i>


                            <h3>Internet </h3>
                            <p>
                                We offer high speed internet services. Get dedicated and shared internet connections at cheap rates.
                            </p>

                        </div>

                    </div>
                    <div class="col-md-3 col-sm-3">

                        <div>

                            <i class="fa fa-gears fa-4x main-color"></i>


                            <h3>Web/ App Development </h3>
                            <p>
                                Develop your own website and grow your business in short time. Feel free to contact us.
                            </p>

                        </div>

                    </div>
                    <div class="col-md-3 col-sm-3">

                        <div>

                            <i class="fa fa-desktop fa-4x main-color"></i>


                            <h3>Camera / Biometric Installation </h3>
                            <p>
                                Secure Your Premises With The Most Advanced Security Equipment.
                            </p>

                        </div>

                    </div>
                    <div class="col-md-3 col-sm-3">

                        <div>

                            <i class="fa fa-folder fa-4x main-color"></i>


                            <h3>Data Center </h3>
                            <p>
                                #comingSOON
                            </p>

                        </div>

                    </div>
                </div>
                <div class="row ">
                    <div class="col-md-6 col-sm-6">
                        <h3>About Gold & Space</h3>
                        <p>
                            An Upcoming ISP |creatives |Branding |Graphic/Motion Design |Photography |Hotspot Installation |App & Web Development |Product endorsement and Marketing

                        </p>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="panel panel-default">

                            <div class="panel-body">
                                <ul class="nav nav-pills" style="background-color: #ECECEC;">
                                    <li class="active"><a href="#home-pills" data-toggle="tab">CYRUS MBITAU</a>
                                    </li>
                                    <li class=""><a href="#profile-pills" data-toggle="tab">MARK KAMANJA</a>
                                    </li>
                                    <li class=""><a href="#messages-pills" data-toggle="tab">SONYANGA MIKE</a>
                                    </li>

                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane fade  active in" id="home-pills">
                                        <h4>C.E.O-FOUNDER</h4>
                                        <p>Mobile: 254792237377<br>Email: CEO@GOLDANDSPACE.CO.KE</p>
                                    </div>
                                    <div class="tab-pane fade" id="profile-pills">
                                        <h4>DIRECTOR</h4>
                                        <p>Mobile: +254719644581<br>MARK@GOLDANDSPACE.CO.KE</p>

                                    </div>
                                    <div class="tab-pane fade" id="messages-pills">
                                        <h4>P.R</h4>
                                        <p>Mobile: +254711433242<br>WEBLEN@GOLDANDSPACE.CO.KE</p>
                                    </div>
                                    <div class="tab-pane fade" id="settings-pills">
                                        <h4>P.R</h4>
                                        <p>Mobile: +254713908493</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>


        </section>
        <!--End Services Section-->
        <!--parallax one-->
        <section id="Parallax-one">
            <div class="container">

                <div class="row text-center">
                    <div class="col-md-8 col-md-offset-2 ">
                        <h2><i class="fa fa-fighter-jet fa-3x" style="color:green"></i>&nbsp;WIwi Team Force </h2>
                        <h4>
                            <strong>"With The Help Of Our Able Partners **TechTitan** and a combination of very low price and decent customer service means that [G&S] are very tough to beat."
                        </strong>
                        </h4>
                    </div>

                </div>


            </div>
        </section>
        <!--./parallax one-->
        <!-- Pricing Section -->
        <section id="price-sec">
            <div class="container">
                <div class="row text-center">
                    <div class="col-md-8 col-md-offset-2">

                        <h2>Pricing Options</h2>
                        <h4>
                            <strong>Given prices of packages are for common connections. For other prices to contact us. 
                        </strong>
                        </h4>
                    </div>

                </div>
                <div class="row text-center">

                    <div class="col-md-12 ">



                        <div class="col-md-3 col-sm-6">
                            <ul class="plan ">
                                <li class="plan-head">DAILY</li>
                                <li class="main-price">Ksh 150 Only</li>
                                <li><strong>10 Mbps </strong>Speeds</li>
                                <li><strong>1</strong>Day Validity</li>
                                <li><strong>24/7 </strong>Customer Support</li>
                                <li class="bottom">
                                    <a href="pay.php?amount=150" class="btn btn-warning">Call to Activate</a>
                                </li>
                            </ul>
                        </div>

                        <div class="col-md-3 col-sm-6">
                            <ul class="plan plan-active">
                                <li class="plan-head head-active ">WEEKLY</li>
                                <li class="main-price">Ksh 400 only</li>
                                <li><strong>10 Mbps </strong>Speeds</li>
                                <li><strong>1 </strong>Week Validity</li>
                                <li><strong>24/7 </strong>Customer Support</li>
                                <li class="bottom">
                                    <a href="pay.php?amount=400" class="btn btn-primary">Call to Activate</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <ul class="plan">
                                <li class="plan-head">MONTHLY</li>
                                <li class="main-price">Ksh 1000 only</li>
                                <li><strong>10 Mbps </strong>Speeds</li>
                                <li><strong>1 </strong>Month Validity</li>
                                <li><strong>24/7 </strong>Customer Support</li>
                                <li class="bottom">
                                    <a href="pay.php?amount=1000" class="btn btn-danger">Call to Activate</a>
                                </li>
                            </ul>
                        </div>

                        <div class="col-md-3 col-sm-6">
                            <ul class="plan">
                                <li class="plan-head">Daily Bundles</li>
                                <li><strong>300MB @</strong>50 Ksh</li>
                                <li><strong>1.2GB @</strong>150 Ksh</li>
                                <li><strong>2.2GB @</strong>250 Ksh</li>
                                <li><strong>5.2GB @</strong>800 Ksh</li>
                                <li class="bottom">
                                    <a href="#contact-sec" class="btn btn-success">Call to Activate</a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>


            </div>
        </section>
        <!--End Pricing Section -->
        <!--parallax two-->
        <section id="Parallax-two">
            <div class="container">

                <div class="row text-center">
                    <div class="col-md-8 col-md-offset-2 ">
                        <h2><i class="fa fa-briefcase fa-3x"></i>&nbsp;Team G&S </h2>
                        <h4>
                            <strong>There’s a lot at stake and you can’t guard against the increasing number of risks alone. Whether you need to assess and respond to cyber threats or mitigate risks and enhance decision making, we’re security and technology experts who’ll help protect your gadgets against a wide range of security threats.
                        </strong>
                        </h4>
                    </div>

                </div>


            </div>
        </section>
        <!--./parallax two-->


        <!-- Contact Section -->
        <section id="contact-sec">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <div id="social-icon">
                            <strong> For Help </strong>KINDLY CALL <strong>+254737935827</strong> <strong> <a href="mailto:info@goldandspace.co.ke">Talk To Us Us</a> </strong>.
                            <!--    <a href="#"><i class="fa fa-facebook fa-2x"></i></a>
                        <a href="#"><i class="fa fa-twitter fa-2x"></i></a>
                        <a href="#"><i class="fa fa-linkedin fa-2x"></i></a>
                        <a href="#"><i class="fa fa-google-plus fa-2x"></i></a>
                        <a href="#"><i class="fa fa-pinterest fa-2x"></i></a> -->
                        </div>
                    </div>


                </div>
            </div>
        </section>

        <!--End Contact Section -->
        <!--footer Section -->
        <footer class="container text-center">
            <span class="text-muted">Powered by <a href="https://www.techtitan.co.ke" target="_blank">TechTitan</a></span>
        </footer>

        <script type="text/javascript">
        <!--
        document.login.username.focus();
        //-->
        </script>
        <!--End footer Section -->
        <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
        <!-- CORE JQUERY  -->
        <script src="assets/plugins/jquery-1.10.2.js"></script>
        <!-- BOOTSTRAP CORE SCRIPT   -->
        <script src="assets/plugins/bootstrap.js"></script>
        <!-- PARALLAX SCRIPT   -->
        <script src="assets/plugins/4jquery.parallax-1.1.3.js"></script>
        <!-- CUSTOM SCRIPT   -->
        <script src="assets/js/custom.js"></script>
        <audio src="music/music.mp3" autoplay="autoplay"></audio>
    </body>

    </html>
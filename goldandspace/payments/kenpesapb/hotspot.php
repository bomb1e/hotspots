<?php
require_once('./AfricasTalkingGateway.php');
require_once('./core/db.conf.php');
/*
the function below will be executed after a successful insert of MPESA transaction into the database
you can use it to carry functions like sending a confirmation SMS using your SMS Gateway
*/

$dbConnection = db_connect('radius');
$hotspotDatabaseConnection = db_connect('hotspot');

// after_success_mpesa($dbConnection, $hotspotDatabaseConnection); // Already called on successful POST

// TODO: More modular function for db connections. Expose as a plugin and import here


function after_success_mpesa($dbConnection, $hotspotDatabaseConnection){
    //do not echo  or print_r - this will interfere with the response format that Kenpesa expect from your server
    
    $postMpesa_msisdn = $_REQUEST['mpesa_msisdn'];
    $postMpesa_amt = $_REQUEST['mpesa_amt'];
    $postMpesa_acc = strtolower(trim($_REQUEST['mpesa_acc']));
    
    // TODO: Get the account from hotspot profile table
    if($postMpesa_acc != 'net100') {
        return;
    }
    
    // Check amount against packages (return: Time in seconds)
    $amount = $postMpesa_amt;
    $expireAfter = getExpiration($amount);
    
    // Generate username & password
    $username = $postMpesa_msisdn;
    $password = randomPassword();
    
    // Delete records matching username in radacct & radcheck
    $radcheckDeleteQuery = "DELETE FROM `radcheck` WHERE `username`='$username';";
    $radacctDeleteQuery = "DELETE FROM `radacct` WHERE `username`='$username';";
    
    // Insert a record into radcheck (params: Time in seconds, Username, Password)
    $simultaneousUseInsertQuery = "INSERT INTO `radcheck`(`username`, `attribute`, `op`, `value`) VALUES ($username, 'Simultaneous-Use', ':=', '1');";
    $passwordInsertQuery = "INSERT INTO `radcheck`(`username`, `attribute`, `op`, `value`) VALUES ($username, 'Cleartext-Password', ':=', '$password');";
    $expirationInsertQuery = "INSERT INTO `radcheck`(`username`, `attribute`, `op`, `value`) VALUES ($username, 'Expire-After', ':=', '$expireAfter')";
    
    /**
    * Insert a record into hotspot users table
    *
    * param             type        description
    *
    * username          string      phone number of the user
    * amount            int         amount the user has paid in KES
    * duration          int         time in seconds given to user based on the rate
    * hotspot_id        int         id of the hotspot that the user belongs to
    */
    
    $newUserQuery= "INSERT INTO `users`(`username`, `amount`, `duration`) VALUES ('$username', '$amount', '$expireAfter')";
    try {
        $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbConnection->beginTransaction();
        $dbConnection->exec($radacctDeleteQuery);
        $dbConnection->exec($radcheckDeleteQuery);
        $dbConnection->exec($passwordInsertQuery);
        $dbConnection->exec($simultaneousUseInsertQuery);
        $dbConnection->exec($expirationInsertQuery);
        $dbConnection->commit();
    } catch(PDOException $Exception) {
        $dbConnection->rollback();
        echo $Exception;
        echo "Unable to create session";
    }
    try {
        $hotspotDatabaseConnection->exec($newUserQuery);
    } catch(PDOException $Exception) {
        $hotstpotDatabaseConnection->rollback();
        echo $Exception;
        echo "Unable to add user";
    }
    // Send SMS with Username and Password and link to login
    $message = "Welcome to Gold and Space!\nYour Username - $username\nYour Password - $password\nUse these details to login to the hotspot.\nCall +254737935827 for support.";
    
    sendSMS($username, $message);
    
    // TODO: Log successful transactions
    
    $dbConnection = null;
}

// TODO: Get rates from hotspot profile table

function getExpiration($amount) {
    $dailyRate = (3600 * 24)/150;
    $weeklyRate = (3600 * 24 * 7)/400;
    $monthlyRate = (3600 * 24 * 31)/1000;
    
    if($amount >= 0 && $amount < 400) {
        return $amount * $dailyRate;
    } elseif($amount >= 400 && $amount < 1000) {
        return $amount * $weeklyRate;
    } elseif($amount >= 1000) {
        return $amount * $monthlyRate;
    }
}

function randomPassword() {
    $alphabet = "123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 5; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}


// TODO: Get Africastalking details from hotspot profile table

function sendSMS($phoneNumber, $message) {
    
    // AFRICASTALKING API DETAILS
    $africasTalkingUsername = 'cyruh';
    $africasTalkingApiKey = '6c8f68b224073cecedbed6604487a182b5cbf2a069c8cb535fa3830bd4c616fb';
    
    // Set recepients
    $recepients = "$phoneNumber";
    
    // Gateway instance
    $gateway = new AfricasTalkingGateway($africasTalkingUsername, $africasTalkingApiKey);
    
    // TODO: Log errors, if insufficient balance, send a notification to hotspot admin
    
    try {
        $results = $gateway->sendMessage($recepients, $message);
    } catch ( AfricasTalkingGatewayException $e ) {
        echo "Failed to send SMS " . $e;
    }
}
?>
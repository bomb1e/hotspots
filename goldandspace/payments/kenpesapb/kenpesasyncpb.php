<?php
require_once("roots.php");
require_once($root_path . 'core/db_connect.php');
require_once($root_path . 'core/functions.php');

require($root_path . 'classes/orm/class.orm.php');

//if (is_readable($root_path . 'hotspot.php')) {
  //  include ($root_path . 'hotspot.php');
//}

//exit("xx");
function check_license($licensekey, $localkey = "") {
    $whmcsurl = "https://www.enetonlinesolutions.co.ke/portal/clients/";
    $licensing_secret_key = "287811c077b90af0b013cddf47dcfd69"; # Unique value, should match what is set in the product configuration for MD5 Hash Verification
    $check_token = time() . md5(mt_rand(1000000000, 9999999999) . $licensekey);
    $checkdate = date("Ymd"); # Current date
    $usersip = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : $_SERVER['LOCAL_ADDR'];
    $localkeydays = 15; # How long the local key is valid for in between remote checks
    $allowcheckfaildays = 5; # How many days to allow after local key expiry before blocking access if connection cannot be made
    $localkeyvalid = false;
    if ($localkey) {
        $localkey = str_replace("\n", '', $localkey); # Remove the line breaks
        $localdata = substr($localkey, 0, strlen($localkey) - 32); # Extract License Data
        $md5hash = substr($localkey, strlen($localkey) - 32); # Extract MD5 Hash
        if ($md5hash == md5($localdata . $licensing_secret_key)) {
            $localdata = strrev($localdata); # Reverse the string
            $md5hash = substr($localdata, 0, 32); # Extract MD5 Hash
            $localdata = substr($localdata, 32); # Extract License Data
            $localdata = base64_decode($localdata);
            $localkeyresults = unserialize($localdata);
            $originalcheckdate = $localkeyresults["checkdate"];
            if ($md5hash == md5($originalcheckdate . $licensing_secret_key)) {
                $localexpiry = date("Ymd", mktime(0, 0, 0, date("m"), date("d") - $localkeydays, date("Y")));
                if ($originalcheckdate > $localexpiry) {
                    $localkeyvalid = true;
                    $results = $localkeyresults;
                    $validdomains = explode(",", $results["validdomain"]);
                    if (!in_array($_SERVER['SERVER_NAME'], $validdomains)) {
                        $localkeyvalid = false;
                        $localkeyresults["status"] = "Invalid";
                        $results = array();
                    }
                    $validips = explode(",", $results["validip"]);
                    if (!in_array($usersip, $validips)) {
                        $localkeyvalid = false;
                        $localkeyresults["status"] = "Invalid";
                        $results = array();
                    }
                    if ($results["validdirectory"] != dirname(__FILE__)) {
                        $localkeyvalid = false;
                        $localkeyresults["status"] = "Invalid";
                        $results = array();
                    }
                }
            }
        }
    }
    if (!$localkeyvalid) {
        $postfields["licensekey"] = $licensekey;
        $postfields["domain"] = $_SERVER['SERVER_NAME'];
        $postfields["ip"] = $usersip;
        $postfields["dir"] = dirname(__FILE__);

        if ($check_token)
            $postfields["check_token"] = $check_token;
        if (function_exists("curl_exec")) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $whmcsurl . "modules/servers/licensing/verify.php");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $data = curl_exec($ch);
            curl_close($ch);
        } else {
            $fp = fsockopen($whmcsurl, 80, $errno, $errstr, 5);
            if ($fp) {
                $querystring = "";
                foreach ($postfields AS $k => $v) {
                    $querystring .= "$k=" . urlencode($v) . "&";
                }
                $header = "POST " . $whmcsurl . "modules/servers/licensing/verify.php HTTP/1.0\r\n";
                $header .= "Host: " . $whmcsurl . "\r\n";
                $header .= "Content-type: application/x-www-form-urlencoded\r\n";
                $header .= "Content-length: " . @strlen($querystring) . "\r\n";
                $header .= "Connection: close\r\n\r\n";

                $header .= $querystring;
                $data = "";
                @stream_set_timeout($fp, 20);
                @fputs($fp, $header);
                $status = @socket_get_status($fp);
                while (!@feof($fp) && $status) {
                    $data .= @fgets($fp, 1024);
                    $status = @socket_get_status($fp);
                }
                @fclose($fp);
            }
        }
        if (!$data) {
            $localexpiry = date("Ymd", mktime(0, 0, 0, date("m"), date("d") - ($localkeydays + $allowcheckfaildays), date("Y")));
            if ($originalcheckdate > $localexpiry) {
                $results = $localkeyresults;
            } else {
                $results["status"] = "Invalid";
                $results["description"] = "Remote Check Failed";
                return $results;
            }
        } else {
            preg_match_all('/<(.*?)>([^<]+)<\/\\1>/i', $data, $matches);
            $results = array();
            foreach ($matches[1] AS $k => $v) {
                $results[$v] = $matches[2][$k];
            }
        }



        if (isset($results["md5hash"])) {
            if ($results["md5hash"] != md5($licensing_secret_key . $check_token)) {
                $results["status"] = "Invalid";
                $results["description"] = "MD5 Checksum Verification Failed";
                return $results;
            }
        }

        if ($results["status"] == "Active") {
            $results["checkdate"] = $checkdate;
            $data_encoded = serialize($results);
            $data_encoded = base64_encode($data_encoded);
            $data_encoded = md5($checkdate . $licensing_secret_key) . $data_encoded;
            $data_encoded = strrev($data_encoded);
            $data_encoded = $data_encoded . md5($data_encoded . $licensing_secret_key);
            $data_encoded = wordwrap($data_encoded, 80, "\n", true);
            $results["localkey"] = $data_encoded;
        }
        $results["remotecheck"] = true;
    }
    unset($postfields, $data, $matches, $whmcsurl, $licensing_secret_key, $checkdate, $usersip, $localkeydays, $allowcheckfaildays, $md5hash);
    return $results;
}

//$lc_result 	= mysql_query("SELECT status, localkey FROM mod_kenpesa WHERE id =1");
//$lc_data	= mysql_fetch_array($lc_result);
//$localkey	= $lc_data['localkey'];
//
//echo "<pre>";
//print_r($license_check);
//echo "</pre>";
//exit();

/* if(isset($license_check['localkey'])){
  if($license_check['localkey'] !== $localkey){
  mysql_query("UPDATE mod_kenpesa SET localkey = '{$license_check['localkey']}'");
  }
  }

  if ($license_check['status'] !== 'Active'){
  die ("Failed|License is {$license_check['status']}");
  }
 */

if (!isset($_REQUEST) || !count($_REQUEST) || empty($_REQUEST['user']) || empty($_REQUEST['user']) || empty($_REQUEST['mpesa_code']) || empty($_REQUEST['business_number'])) {
    die("Failed|Empty Request or missing values");
}

$postUser = isset($_REQUEST['user']) ? $_REQUEST['user'] : null;
$postPass = isset($_REQUEST['pass']) ? $_REQUEST['pass'] : null;
$postOrig = isset($_REQUEST['orig']) ? $_REQUEST['orig'] : null;
$postId = isset($_REQUEST['id']) ? $_REQUEST['id'] : null;
$postDest = isset($_REQUEST['dest']) ? $_REQUEST['dest'] : null;
$postTstamp = isset($_REQUEST['tstamp']) ? $_REQUEST['tstamp'] : null;
$postText = isset($_REQUEST['text']) ? $_REQUEST['text'] : null;
$postMpesa_code = isset($_REQUEST['mpesa_code']) ? $_REQUEST['mpesa_code'] : null;
$postMpesa_acc = isset($_REQUEST['mpesa_acc']) ? $_REQUEST['mpesa_acc'] : 0;
$postMpesa_msisdn = isset($_REQUEST['mpesa_msisdn']) ? $_REQUEST['mpesa_msisdn'] : null;
$postMpesa_trx_date = isset($_REQUEST['mpesa_trx_date']) ? $_REQUEST['mpesa_trx_date'] : null;
$postMpesa_trx_time = isset($_REQUEST['mpesa_trx_time']) ? $_REQUEST['mpesa_trx_time'] : null;
$postMpesa_amt = isset($_REQUEST['mpesa_amt']) ? $_REQUEST['mpesa_amt'] : null;
$postMpesa_sender = isset($_REQUEST['mpesa_sender']) ? $_REQUEST['mpesa_sender'] : null;
$postOrgAccountBalance = isset($_REQUEST['OrgAccountBalance']) ? $_REQUEST['OrgAccountBalance'] : null;
$postbusiness_number = isset($_REQUEST['business_number']) ? $_REQUEST['business_number'] : null;
$debug = (isset($_REQUEST['debug']) && $_REQUEST['debug'] == 1) ? 1 : 0;

$db->debug = $debug;
//$db->debug = 1;

$local_config = $db->GetRow("SELECT * FROM tbllicenses WHERE business_number = '{$postbusiness_number}'");


//print_pre($local_config);exit;

$user = "";
$pass = "";

$licensekey = '';
$localkey = '';

if (!count($local_config)) {
    die("Failed|License configuration for {$postbusiness_number} not found.");
} else {
    require_once($root_path . 'classes/adodb/adodb-errorhandler.inc.php');
    require_once($root_path . 'classes/adodb/adodb.inc.php');
    require_once($root_path . 'classes/adodb/adodb-active-record.inc.php');


    if (!defined('ADODB_ERROR_LOG_TYPE'))
        define('ADODB_ERROR_LOG_TYPE', 3);
    if (!defined('ADODB_ERROR_LOG_DEST'))
        define('ADODB_ERROR_LOG_DEST', $root_path . 'logs/' . date('Ymd') . '_db_errors.log');


    $db_type = isset($local_config['dbtype']) ? $local_config['dbtype'] : '';
    $db_host = isset($local_config['dbhost']) ? $local_config['dbhost'] : '';
    $db_username = isset($local_config['dbuser']) ? $local_config['dbuser'] : '';
    $db_password = isset($local_config['dbpass']) ? $local_config['dbpass'] : '';
    $db_dbname = isset($local_config['dbname']) ? $local_config['dbname'] : '';


    $user = isset($local_config['apiuser']) ? $local_config['apiuser'] : '';
    $pass = isset($local_config['apipass']) ? $local_config['apipass'] : '';
    $licensekey = isset($local_config['license']) ? $local_config['license'] : '';
    $localkey = isset($local_config['localkey']) ? $local_config['localkey'] : '';



    $db_store = ADONewConnection($db_type);

    $connect_store = $db_store->Connect($db_host, $db_username, $db_password, $db_dbname);


    if (!$connect_store) {
        die("Failed|Database connection failed for the business account {$postbusiness_number} - " . $db->ErrorMsg());
    } else {

        $db_store->SetFetchMode(ADODB_FETCH_ASSOC);
        ADOdb_Active_Record::SetDatabaseAdapter($db_store);
    }


    $db_store->debug = $debug;

    $license_check = check_license($licensekey, $localkey);

//   print_r($license_check);

    if (isset($license_check["remotecheck"]) && $license_check["remotecheck"]) {

        $update = array();

        $update['localkey'] = $license_check["localkey"];

        $db->AutoExecute('tbllicenses', $update, 'UPDATE', "business_number = '{$postbusiness_number}'");
    }

    if ($license_check['status'] !== 'Active') {
        die("Failed|License is {$license_check['status']}");
    }


    $err = '';

    if (!empty($user)) {

        if (empty($postUser)) {
            $err = $err . " Username Required";
        } else {
            if ($postUser != $user) {
                $err = $err . " Username didn't match.";
            }
        }
    }

    if (!empty($pass)) {
        if (empty($postPass)) {
            $err = $err . " Password Required";
        } else {
            if ($postPass != $pass) {
                $err = $err . " Password didn't match.";
            }
        }
    }

    if ($err != "") {
        die("Failed|{$postMpesa_code} - Invalid or missing IPN login details");
    }


    $record = new ORM('tblpbtransactions', array('mpesa_code'));

    $record->Load("mpesa_code = '{$postMpesa_code}'");

    if (!empty($record->_original)) {
        die("Failed|{$postMpesa_code} - Record exists.");
    } else {
        $record->id = $postId;
        $record->orig = $postOrig;
        $record->dest = $postDest;
        $record->tstamp = $postTstamp;
        $record->text = $postText;
        $record->mpesa_code = $postMpesa_code;
        $record->mpesa_acc = $postMpesa_acc;
        $record->mpesa_msisdn = $postMpesa_msisdn;
        $record->mpesa_trx_date = $postMpesa_trx_date;
        $record->mpesa_trx_time = $postMpesa_trx_time;
        $record->mpesa_amt = $postMpesa_amt;
        $record->mpesa_sender = $postMpesa_sender;
        $record->business_number = $postbusiness_number;
        $record->OrgAccountBalance = $postOrgAccountBalance;
        $record->status = 'Open';
      $record->invoiceid = (int) $postMpesa_acc;
        $record->archivedate = (date("Y") + 5) . date("-m-d");

        if ($record->Save()) {

if (is_readable($root_path . 'hotspot.php')) {
    include ($root_path . 'hotspot.php');
}

            if (function_exists('after_success_mpesa')) {
//                $dbConnection = db_connect('radius');
  //              $hotspotDatabaseConnection = db_connect('hotspot');
                after_success_mpesa($dbConnection, $hotspotDatabaseConnection);
//                @after_success_mpesa();
                echo "OK|{$postMpesa_code} - API Successful";
            }
        } else {
            echo ("Failed|{$postMpesa_code} - An error occurred during save.");
        }
    }
}
?>

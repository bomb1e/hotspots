<?php
ini_set('display_errors',1);

include('roots.php');

require ($root_path."core/db.conf.php");

require_once($root_path . 'classes/adodb/adodb-errorhandler.inc.php');
require_once($root_path . 'classes/adodb/adodb.inc.php');
require_once($root_path . 'classes/adodb/adodb-active-record.inc.php');


if(!defined('ADODB_ERROR_LOG_TYPE')) define('ADODB_ERROR_LOG_TYPE',3);
if(!defined('ADODB_ERROR_LOG_DEST')) define('ADODB_ERROR_LOG_DEST',$root_path.'logs/'.date('Ymd').'_db_errors.log');


$db_type      = isset($config['db_type']) ? $config['db_type'] : '';
$db_host      = isset($config['db_host']) ? $config['db_host'] : '';
$db_username  = isset($config['db_username']) ? $config['db_username'] : '';
$db_password  = isset($config['db_password']) ? $config['db_password'] : '';
$db_dbname    = isset($config['db_dbname']) ? $config['db_dbname'] : '';

$db = ADONewConnection($db_type);

//print_r($config);

$connect = $db->Connect($db_host, $db_username, $db_password, $db_dbname);

if(!$connect){
  die("Database connection failed");
}else{

 $db->SetFetchMode(ADODB_FETCH_ASSOC);
 ADOdb_Active_Record::SetDatabaseAdapter($db);
 
}


?>

<?php
require_once('./AfricasTalkingGateway.php');
/*
the function below will be executed after a successful insert of MPESA transaction into the database
you can use it to carry functions like sending a confirmation SMS using your SMS Gateway
*/
$dbConnection = db_connect();
after_success_mpesa();
function db_connect(){

	$dsn = 'mysql:dbname=syk_radius;host=127.0.0.1';
	$user = 'root';
	$password = '%_TECH_TITAN_%';

	$dbh = new PDO($dsn, $user, $password);

	try 
	{
	    $connection = new PDO($dsn, $user, $password);
	return $connection;
	}
	catch( PDOException $Exception ) 
	{   
	     echo "Unable to connect to database.";
	     exit;
	}
}
function after_success_mpesa(){
//do not echo  or print_r - this will interfere with the response format that Kenpesa expect from your server

	$postMpesa_msisdn = $_REQUEST['mpesa_msisdn'];
	$postMpesa_amt = $_REQUEST['mpesa_amt'];

	// Check amount against packages (return: Time in seconds)
	$amount = $postMpesa_amt;
	$expireAfter = getExpiration($amount);

	// Generate username & password
	$username = $postMpesa_msisdn;
	$password = randomPassword();

	// Delete records matching username in radacct & radcheck
	$radcheckDeleteQuery = "DELETE FROM `radcheck` WHERE `username`=$username";
	$radacctDeleteQuery = "DELETE FROM `radacct` WHERE `username`=$username";
	
	// Insert a record into radcheck (params: Time in seconds, Username, Password)
	$simultaneousUseInsertQuery = "INSERT INTO `radcheck`(`username`, `attribute`, `op`, `value`) VALUES ($username, 'Simultaneous-Use', ':=', '1')";
	$passwordInsertQuery = "INSERT INTO `radcheck`(`username`, `attribute`, `op`, `value`) VALUES ($username, 'Cleartext-Password', ':=', $password)";
	$expirationInsertQuery = "INSERT INTO `radcheck`(`username`, `attribute`, `op`, `value`) VALUES ($username, 'Expire-After', ':=', $expireAfter)";
	try {
		$dbConnection->beginTransaction();
		$dbConnection->exec($radcheckDeleteQuery);
		$dbConnection->exec($radacctDeleteQuery);	
		$dbConnection->exec($simultaneousUseInsertQuery);
		$dbConnection->exec($passwordInsertQuery);
		$dbConnection->exec($expirationInsertQuery);
		$dbConnection->commit();
	} catch(PDOException $Exception) {
		$dbConnection->rollback();
		echo "Unable to connect to database";
	}

	// Send SMS with Username and Password and link to login
	$message = "Welcome to the Fiberlink Network!\nYour Username - $username\nYour Password - $password\nUse these details to login to the hotspot.\nCall +25471223456 for support.";
	
	sendSMS($username, $message);

	$dbConnection = null;
}

function getExpiration($amount) {
	$hourlyRate = 3600/20;
	$dailyRate = (3600 * 24)/200;
	$weeklyRate = (3600 * 24 * 7)/500;
	$monthlyRate = (3600 * 24 * 31)/1500;	

	if($amount > 0 && $amount < 200) {
		return $amount * $hourlyRate;
	} elseif($amount >= 200 && $amount < 500) {
		return $amount * $dailyRate;
	} elseif($amount >= 500 && $amount < 1500) {
		return $amount * $weeklyRate;
	} elseif($amount >= 1500) {
		return $amount * $monthlyRate;
	}
}

function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function sendSMS($phoneNumber, $message) {
	$africasTalkingUsername = 'syokinet';
	$africasTalkingApiKey = 'fb7349995a1323f6c681a1622cc6411d684f04eda740d3201ec3abdef7aa7cf2';
	
	// Set recepients
	$recepients = "$phoneNumber";
	
	// Gateway instance
	$gateway = new AfricasTalkingGateway($africasTalkingUsername, $africasTalkingApiKey);
	
	try {
		$results = $gateway->sendMessage($recepients, $message);
	} catch ( AfricasTalkingGatewayException $e ) {
		
	}
}
?>

<?php
require_once('./AfricasTalkingGateway.php');
require_once('./core/db.conf.php');
/*
the function below will be executed after a successful insert of MPESA transaction into the database
you can use it to carry functions like sending a confirmation SMS using your SMS Gateway
*/
$dbConnection = db_connect('radius');
$hotspotDatabaseConnection = db_connect('hotspot');
//after_success_mpesa($dbConnection, $hotspotDatabaseConnection); //already called on successful insert
/*
moved this to core/db.conf.php
function db_connect($database){

	$dsn = 'mysql:dbname='.$database.';host=127.0.0.1';
	$user = 'app_user';
	$password = '%_TECH_TITAN_%';

	try 
	{
	    $connection = new PDO($dsn, $user, $password);
	    return $connection;
	}
	catch( PDOException $Exception ) 
	{   
	     echo $Exception;
	     exit;
	}
}
*/
function after_success_mpesa($dbConnection, $hotspotDatabaseConnection){
//do not echo  or print_r - this will interfere with the response format that Kenpesa expect from your server

	$postMpesa_msisdn = $_REQUEST['mpesa_msisdn'];
	$postMpesa_amt = $_REQUEST['mpesa_amt'];
	$postMpesa_acc = $_REQUEST['mpesa_acc'];
	
	if($postMpesa_acc != '100100') {
		return;
	}

	// Check amount against packages (return: Time in seconds)
	$amount = $postMpesa_amt;
	$expireAfter = getExpiration($amount);

	// Generate username & password
	$username = $postMpesa_msisdn;
	$password = randomPassword();

	// Delete records matching username in radacct & radcheck
	$radcheckDeleteQuery = "DELETE FROM `radcheck` WHERE `username`='$username';";
	$radacctDeleteQuery = "DELETE FROM `radacct` WHERE `username`='$username';";
	
	// Insert a record into radcheck (params: Time in seconds, Username, Password)
	$simultaneousUseInsertQuery = "INSERT INTO `radcheck`(`username`, `attribute`, `op`, `value`) VALUES ($username, 'Simultaneous-Use', ':=', '1');";
	$passwordInsertQuery = "INSERT INTO `radcheck`(`username`, `attribute`, `op`, `value`) VALUES ($username, 'Cleartext-Password', ':=', '$password');";
	$expirationInsertQuery = "INSERT INTO `radcheck`(`username`, `attribute`, `op`, `value`) VALUES ($username, 'Expire-After', ':=', '$expireAfter')";
	$newUserQuery= "INSERT INTO `users`(`username`, `amount`, `duration`) VALUES ('$username', '$amount', '$expireAfter')";
	try {
		$dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$dbConnection->beginTransaction();
		$dbConnection->exec($radacctDeleteQuery);
		$dbConnection->exec($radcheckDeleteQuery);
		$dbConnection->exec($passwordInsertQuery);
		$dbConnection->exec($simultaneousUseInsertQuery);
		$dbConnection->exec($expirationInsertQuery);
		$dbConnection->commit();
	} catch(PDOException $Exception) {
		$dbConnection->rollback();
		echo $Exception;
		echo "Unable to create session";
	}
	try {
		$hotspotDatabaseConnection->exec($newUserQuery);
	} catch(PDOException $Exception) {
		$hotstpotDatabaseConnection->rollback();
		echo $Exception;
		echo "Unable to add user";
	}
	// Send SMS with Username and Password and link to login
	$message = "Welcome to the Fiberlink Network!\nYour Username - $username\nYour Password - $password\n Go to Use these details to login to the hotspot.\nCall +254202000501 for support.";
	
	sendSMS($username, $message);

	$dbConnection = null;
}

function getExpiration($amount) {
	$hourlyRate = 3600/30;
	$dailyRate = (3600 * 24)/100;
	$weeklyRate = (3600 * 24 * 7)/500;
	$monthlyRate = (3600 * 24 * 31)/1000;	

	if($amount > 0 && $amount < 100) {
		return $amount * $hourlyRate;
	} elseif($amount >= 100 && $amount < 500) {
		return $amount * $dailyRate;
	} elseif($amount >= 500 && $amount < 1000) {
		return $amount * $weeklyRate;
	} elseif($amount >= 1000) {
		return $amount * $monthlyRate;
	}
}

function randomPassword() {
    $alphabet = "123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 5; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function sendSMS($phoneNumber, $message) {
	//$africasTalkingUsername = 'syokinet';
	//$africasTalkingApiKey = 'fb7349995a1323f6c681a1622cc6411d684f04eda740d3201ec3abdef7aa7cf2';
	
	// FIBERLINK DETAILS
	$africasTalkingUsername = 'FIBERLINK';
	$africasTalkingApiKey = '9b23dcfe69b09478ef774202c1d11a80526d12668bcd6cf93f75ec9e32efdf71';
	
	// Set recepients
	$recepients = "$phoneNumber";
	
	// Gateway instance
	$gateway = new AfricasTalkingGateway($africasTalkingUsername, $africasTalkingApiKey);

	try {
		$results = $gateway->sendMessage($recepients, $message);
	} catch ( AfricasTalkingGatewayException $e ) {
		echo "Failed to send SMS " . $e;		
	}
}
?>

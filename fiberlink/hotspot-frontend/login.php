<?php
    session_start();
    if(isset($_POST) && !empty($_POST)) {
        $_SESSION['mac'] = $_POST['mac'];
        $_SESSION['ip'] = $_POST['ip'];
        $_SESSION['username'] = $_POST['username'];
        $_SESSION['link-login'] = $_POST['link-login'];
        $_SESSION['link-orig'] = $_POST['link-orig'];
        $_SESSION['error'] = $_POST['error'];
        $_SESSION['chap-id'] = $_POST['chap-id'];
        $_SESSION['chap-challenge'] = $_POST['chap-challenge'];
        $_SESSION['link-login-only'] = $_POST['link-login-only'];
        $_SESSION['link-orig-esc'] = $_POST['link-orig-esc'];
        $_SESSION['mac-esc'] = $_POST['mac-esc'];
    }

    $mac=$_SESSION['mac'];
    $ip=$_SESSION['ip'];
    $username=$_SESSION['username'];
    $linklogin=$_SESSION['link-login'];
    $linkorig=$_SESSION['link-orig'];
    $error=$_SESSION['error'];
    $chapid=$_SESSION['chap-id'];
    $chapchallenge=$_SESSION['chap-challenge'];
    $linkloginonly=$_SESSION['link-login-only'];
    $linkorigesc=$_SESSION['link-orig-esc'];
    $macesc=$_SESSION['mac-esc'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Login | Internet Hotspot</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="expires" content="-1" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
    <link rel="stylesheet" type="text/css" href="./assets/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="./assets/style.css">
</head>

<body>

    <form name="sendin" action="<?php echo $linkloginonly; ?>" method="post">
        <input type="hidden" name="username" />
        <input type="hidden" name="password" />
        <input type="hidden" name="dst" value="<?php echo $linkorig; ?>" />
        <input type="hidden" name="popup" value="true" />
    </form>
    
    <script type="text/javascript" src="./md5.js"></script>
    <script type="text/javascript">
        function doLogin() {
                <?php if(strlen($chapid) < 1) echo "return true;\n"; ?>
        document.sendin.username.value = document.login.username.value;
        document.sendin.password.value = hexMD5('<?php echo $chapid; ?>' + document.login.password.value + '<?php echo $chapchallenge; ?>');
        document.sendin.submit();
        return false;
        }
    </script>

    <header>
       <nav class="navbar navbar-default" style="background: white; color: #444;">
            <div class="container">
                <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">
                    <img src="./assets/fiberlinklogo.png" height="100%">
                </a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="index.php">Home</a></li>
                </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>
    </header>

    <main class="container">
        <div class="jumbotron text-center" style="background: #3477b4; color: white;">
            <p>
                Please log in to use the internet hotspot service. <br>
                Call <a href="tel:+254202000501" style="color:white; font-weight:bold;">+254202000501</a> for help
            </p>
        </div>
        <?php if(isset($error) && !empty($error)) { ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $error; ?>
        </div>
        <?php } ?>
        <form class="panel panel-default col-md-4 col-md-offset-4" name="login" action="<?php echo $linkloginonly; ?>" method="post" onSubmit="return doLogin()">
            <div class="panel-body">
                <input type="hidden" name="dst" value="<?php echo $linkorig; ?>"/>
                <input type="hidden" name="popup" value="true" />

                <div class="form-group">
                    <label>Username</label>
                    <input name="username" class="form-control" type="text" value="<?php echo $username; ?>" required/>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input class="form-control" name="password" type="password" required/>
                </div>
                <button class="btn btn-primary btn-lg btn-block" value="OK" type="submit">LOGIN</button>
            </div>
        </form>
        <div class="col-md-4 col-md-offset-4">
        <a class="btn btn-success btn-lg btn-block" href="index.php">Buy an Unlimited Package</a>
        </div>
    </main>
<script type="text/javascript">
<!--
  document.login.username.focus();
//-->
</script>
<script src="./assets/jquery.min.js"></script>
<script src="./assets/bootstrap/bootstrap.min.js"></script>
</body>
</html>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Pay | Internet Hotspot</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="./assets/bootstrap/bootstrap.min.css">
    <style>
        html, body {
            margin: 0;
            padding: 0;
        }
        body {
            padding: 70px 0;
        }
        .center-block {
            display: block;
            margin: 0 auto;
        }
    </style>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="container">
           <div class="row">
                <div>
                    <nav class="navbar navbar-default navbar-fixed-top" style="background: white; color: #444;">
                        <div class="container">
                            <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="index.php">
                                <img src="./assets/fiberlinklogo.png" height="100%">
                            </a>
                            </div>
                            <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="index.php">Home</a></li>
                            </ul>
                            </div><!--/.nav-collapse -->
                        </div>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="jumbotron" style="background: #3477b4; color: white; line-height: 2;">
                        <ul>
                            <li>Go to MPESA</li>
                            <li>Select Lipa na Mpesa</li>
                            <li>Go to Paybill</li>
                            <li>Enter Business number <b><h3 style="display: inline;">752626</h3></b></li>
                            <li>Enter <b><h3 style="display: inline;">100100</h3></b> as the Account Number</li>
                            <li>Enter <h3 style="display: inline;"><strong id="amount"></strong></h3> as the amount</li>
                            <li>Enter your MPESA pin and pay</li>
                            <li>Once payment is successful, you will get a <u>username</u> and <u>password</u> via <b>SMS</b>. These are the details you will use for the duration of your package. </li>
                        </ul>
                    </div>
                    <a class="btn btn-success btn-lg btn-block" href="./login.php">Proceed to Login</a>
                </div>
            </div>
        </div>
    </body>
    <script>
        (function() {
            var amount = document.querySelector('#amount');
            amount.innerHTML = window.location.href.split('=').pop()
        })();
    </script>
    <script src="./assets/jquery.min.js"></script>
    <script src="./assets/bootstrap/bootstrap.min.js"></script>

</html>